FROM python:3.10-alpine

# RUN apk add jupyter-notebook
RUN apk add build-base python3-dev libffi-dev linux-headers musl-dev gcc
RUN pip install --upgrade pip && pip install jupyter

WORKDIR /code
COPY .  .
RUN python setup.py install
RUN install_echo_kernel


CMD jupyter notebook --ip=0.0.0.0 --port=8888 --allow-root --NotebookApp.token=''