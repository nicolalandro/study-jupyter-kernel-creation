from setuptools import setup

setup(
      name='echo_kernel',
      version='0.0.1',
      description='Hello world jupyter kernel Jupyter',
      author='Nicola Landro',
      author_email='nicolaxx94@live.it',
      license='MIT',
      classifiers=[
          'License :: OSI Approved :: MIT License',
      ],
      url='https://gitlab.com/nicolalandro/study-jupyter-kernel-creation',
      download_url='https://gitlab.com/nicolalandro/study-jupyter-kernel-creation',
      packages=['echo_kernel'],
      scripts=['echo_kernel/install_echo_kernel'],
      keywords=['jupyter', 'notebook', 'kernel', 'echo'],
      include_package_data=True
)