[![pipeline status](https://gitlab.com/nicolalandro/study-jupyter-kernel-creation/badges/main/pipeline.svg)](https://gitlab.com/nicolalandro/study-jupyter-kernel-creation/-/commits/main) 
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/nicolalandro%2Fstudy-jupyter-kernel-creation/main)

# Jupyter Echo kernel

A simple study to how to write a jupyter kernel.

## How to run

```
docker compose up
# localhost:8888
```

## How to install

```
pip install echo-kernel --index-url https://gitlab.com/api/v4/projects/47959778/packages/pypi/simple
install_echo_kernel
# run you jupyter and test it
jupyter notebook
```

# References
* docker
* python
* jupyter
* [Making Simple python wrapper kernels](https://jupyter-client.readthedocs.io/en/latest/wrapperkernels.html): explanation
* [Minimal C kernel](https://github.com/brendan-rius/jupyter-c-kernel/tree/master): code example
* [Bash kernel](https://github.com/takluyver/bash_kernel/tree/master): code example